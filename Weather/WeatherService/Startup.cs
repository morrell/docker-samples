﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Pivotal.Discovery.Client;
using WeatherService.Services;
using Steeltoe.Management.Endpoint.CloudFoundry;
using WeatherService.Models;

namespace WeatherService
{
    public class Startup
    {
        public IConfiguration Configuration { get; private set; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            // Add Steeltoe Management services
            services.AddCloudFoundryActuator(Configuration);

            services.AddDiscoveryClient(Configuration);

            services.AddCors(o => o.AddPolicy("AnyPolicy", builder =>
                {
                builder.AllowAnyOrigin()
                   .AllowAnyMethod()
                    .AllowAnyHeader();
                }));

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            // Add Distributed tracing
            // services.AddDistributedTracing(Configuration);

            // Export traces to Zipkin
            // services.AddZipkinExporter(Configuration);
            services.AddTransient<IForecasts, StaticForecasts>();


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            // Add Steeltoe Management endpoints into pipeline
            app.UseCloudFoundryActuator();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
            }

            app.UseCors("AnyPolicy");
            
            app.UseMvc();

            

            // Start Steeltoe Discovery services
            app.UseDiscoveryClient();
        }
    }
}
