﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WeatherService.Models;

namespace WeatherService.Services
{
    public interface IForecasts
    {
        Forecast GetForecast(DateTime StartDate);
        IEnumerable<Forecast> GetForecastRange(DateTime StartDate, DateTime EndDate);
        IEnumerable<Forecast> GetForecastsAscending(int MaxCount=0);
        IEnumerable<Forecast> GetForecastsDescending(int MaxCount=0);
    }

    public class StaticForecasts : IForecasts
    {
        IEnumerable<Forecast> _staticForecasts;

        public StaticForecasts()
        {
            _staticForecasts = new List<Forecast>
            {
                new Forecast { Date = DateTime.Now, Id = 1, Temperatures = new int[] { 71, 72, 73 } },
                new Forecast { Date = DateTime.Now.AddDays(1).Date, Id = 2, Temperatures = new int[] { 75, 76, 78 } },
                new Forecast { Date = DateTime.Now.AddDays(2).Date, Id = 3, Temperatures = new int[] { 81, 82, 84 } }
            };
        }

        public Forecast GetForecast(DateTime StartDate)
        {
            return new Forecast { Date = DateTime.Now.Date, Id = 1, Temperatures = new int[] { 71, 72, 73 } };
        }

        public IEnumerable<Forecast> GetForecastRange(DateTime StartDate, DateTime EndDate)
        {
            return _staticForecasts;
        }

        public IEnumerable<Forecast> GetForecastsAscending(int MaxCount)
        {
            if (MaxCount > 0)
            {
                return _staticForecasts.Take(MaxCount);
            }
            else
            {
                return _staticForecasts;
            }
        }

        public IEnumerable<Forecast> GetForecastsDescending(int MaxCount)
        {
            if (MaxCount > 0)
            {
                return _staticForecasts.OrderByDescending(f => f.Date).Take(MaxCount);
            }
            else {
                return _staticForecasts.OrderByDescending(f => f.Date);
            }
        }
    }
}
