﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WeatherService.Models
{
    public class Forecast
    {
        public long Id { get; set; }
        public DateTime Date { get; set; }
        public int[] Temperatures { get; set; }
        // public float Percipitation { get; set; }
        // public float Humidity { get; set; }
    }
}
