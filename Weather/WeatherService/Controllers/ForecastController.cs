﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WeatherService.Models;
using WeatherService.Services;

namespace WeatherService.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ForecastController : ControllerBase
    {
        private IForecasts _forecasts;

        public ForecastController(IForecasts forecasts)
        {
            _forecasts = forecasts;
        }

        [HttpGet]
        public IEnumerable<Forecast> Get()
        {
            return _forecasts.GetForecastsAscending();
        }

        [HttpGet("{date}")]
        public ActionResult<Forecast> Get(DateTime date)
        {
            return _forecasts.GetForecast(date);
        }

        [HttpPost]
        public ActionResult Post([FromBody] string value)
        {
            return new OkResult();
        }

        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] string value)
        {
            return new OkResult();
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            return new OkResult();
        }
    }
}
