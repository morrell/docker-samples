# Getting started with Docker and Dotnet Core

This guide is not intended to be a whole-hearted approach to learning docker or dotnet core by any means and some knowledge of docker is highly recommended before reading.

## The Dockerfile
The dockerfile will contain the instructions for building and testing the project. It can be used to define a highly customizable platform onto which you can execute tasks in various forms.

To create a basic Dockerfile, simply make an empty text file called (literally) **Dockerfile**  

**Note:** a Dockerfile has no file extension

To start we want to base our build platform off of the node image hosted on the public dockerhub registry.

```
FROM microsoft/dotnet:sdk
```

## Test specific notes
To run browser tests in a docker environment a 'headless' browser option is required.

The latest versions of the chrome browser come with a headless mode.

To use the Chrome browser in the image, install it on the platform directly.

```
FROM microsoft/dotnet:sdk

# Install Google Chrome
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add -

RUN sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list'

RUN apt-get update && apt-get install -y google-chrome-stable
```

If using Angular for a front-end within the project read through the Angular README.md material in this repository for information on configuring Karma.