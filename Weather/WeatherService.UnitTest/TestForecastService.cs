using System;
using Xunit;

using WeatherService.Services;
using WeatherService.Models;
using System.Collections.Generic;

namespace WeatherService.UnitTest
{
    public class TestForecastService
    {

        private readonly StaticForecasts _forecasts;

        public TestForecastService()
        {
            _forecasts = new StaticForecasts();
        }

        [Fact]
        public void IsNotEmpty()
        {
            // Arrange
            var result = _forecasts.GetForecastsAscending(1);

            // Act

            // Assert
            Assert.NotEmpty(result);
        }
    }
}
