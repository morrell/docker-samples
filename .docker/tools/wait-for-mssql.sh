#!/bin/sh
# wait-for-mssql.sh

set -e

server="$1"
password="$2"
shift
cmd="$@"

until /opt/mssql-tools/bin/sqlcmd -S $1 -U sa -P $2 -Q "SELECT 1"; do
  >&2 echo "Database is unavailable - sleeping"
  sleep 1
done

>&2 echo " is up - executing command"

exec $cmd