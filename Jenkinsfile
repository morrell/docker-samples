pipeline {
    agent any
    triggers {
        pollSCM 'H/2 * * * *'
    }
    environment {
        CI_REGISTRY = "docker.corp.vizientinc.com"
        HELM_REPO = "https://artifacts.corp.vizientinc.com/artifactory/helm"
    }
    stages {
        stage('Build Images') {    
            parallel {
                stage('Build Dashboard'){
                    agent {
                        label "linux"
                    }
                    steps {
                        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'Artifactory',
                        usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                            sh '''
                                COMPOSENAME=dashboard
                                CHARTNAME=sample-dashboard
                                VERSION=1.0.1
                                docker login -u $USERNAME -p $PASSWORD https://${CI_REGISTRY}
                                docker-compose build ${COMPOSENAME}
                                docker-compose push ${COMPOSENAME}
                                docker run -v "$(pwd)/charts/:/charts" docker.corp.vizientinc.com/helm helm package --app-version="${VERSION}" --version="${VERSION}" /charts/${CHARTNAME} -d /charts/ --save=false
                                curl -u ${USERNAME}:${PASSWORD} -T charts/${CHARTNAME}-${VERSION}.tgz "${HELM_REPO}/${CHARTNAME}-${VERSION}.tgz"
                            '''
                        }
                    }
                }
                stage('Build Greeter'){
                    agent {
                        label "linux"
                    }
                    steps {
                        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'Artifactory',
                        usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                            sh '''
                                COMPOSENAME=greeter
                                CHARTNAME=sample-greeter-service
                                VERSION=1.0.1
                                docker login -u $USERNAME -p $PASSWORD https://${CI_REGISTRY}
                                docker-compose build ${COMPOSENAME}
                                docker-compose push ${COMPOSENAME}
                                docker run -v "$(pwd)/charts/:/charts" docker.corp.vizientinc.com/helm helm package --app-version="${VERSION}" --version="${VERSION}" /charts/${CHARTNAME} -d /charts/ --save=false
                                curl -u ${USERNAME}:${PASSWORD} -T charts/${CHARTNAME}-${VERSION}.tgz "${HELM_REPO}/${CHARTNAME}-${VERSION}.tgz"
                            '''
                        }
                    }
                }
                stage('Build Weather'){
                    agent {
                        label "linux"
                    }
                    steps {
                        withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: 'Artifactory',
                        usernameVariable: 'USERNAME', passwordVariable: 'PASSWORD']]) {
                            sh '''
                                COMPOSENAME=weather
                                CHARTNAME=sample-weather-service
                                VERSION=1.0.1
                                docker login -u $USERNAME -p $PASSWORD https://${CI_REGISTRY}
                                docker-compose build ${COMPOSENAME}
                                docker-compose push ${COMPOSENAME}
                                docker run -v "$(pwd)/charts/:/charts" docker.corp.vizientinc.com/helm helm package --app-version="${VERSION}" --version="${VERSION}" /charts/${CHARTNAME} -d /charts/ --save=false
                                curl -u ${USERNAME}:${PASSWORD} -T charts/${CHARTNAME}-${VERSION}.tgz "${HELM_REPO}/${CHARTNAME}-${VERSION}.tgz"
                            '''
                        }
                    }
                }
            }
        } 
    }
    post{
        always{
            sh 'docker-compose down'
            echo 'Build completed...'
        }
    }
}