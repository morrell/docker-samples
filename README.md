# Getting Docker and Docker Compose

Docker is awesome and there's no doubt you want to use it.

To get started, here are some good introductory resources.

- Placeholder
- Placeholder
- Placeholder

This guide is not intended to be a whole-hearted approach to learning docker or angular by any means and some knowledge of docker is highly recommended before reading.

## Recommended

Feel free to just dig through the projects, however, each folder is intended to give specific steps on how to build each type of application in it's own way.

- link to Angular readme
- link to Dotnet readme
- link to Java readme

After understanding how to build application images the Compose documentation will show the use of **docker-compose** to accomplish the task of building, unit testing and producing container artifacts that can then be used to orchestrate more elaborate environments to conduct sophistacted tasks.

- Link to docker compose readme