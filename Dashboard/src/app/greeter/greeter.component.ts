import { Component, OnInit } from '@angular/core';
import { GreeterService } from '../services/greeter.service';
import { Greeting } from '../models/greeting';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-greeter',
  templateUrl: './greeter.component.html',
  styleUrls: ['./greeter.component.css']
})
export class GreeterComponent implements OnInit {

  private greeting: Greeting;

  constructor(private greeter: GreeterService) {
  }

  ngOnInit() {
    this.greeter.SayHello().subscribe(
      data => { this.greeting = data; },
      err => console.error(err),
      () => console.log('Done loading greeting')
    );
  }

}
