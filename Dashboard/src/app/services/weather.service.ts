import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Forecast } from '../models/forecast';

@Injectable({
  providedIn: 'root'
})
export class WeatherService {

  private _Forecasts: BehaviorSubject<Forecast[]>;

  private dataStore: {
    Forecasts: Forecast[]
  };

  constructor(private http: HttpClient) {
    this.dataStore = { Forecasts: [] };
    this._Forecasts = new BehaviorSubject<Forecast[]>([]);
   }

   get Forecasts(): Observable<Forecast[]> {
     return this._Forecasts.asObservable();
   }

   ForecastById(id: number) {
    return this.dataStore.Forecasts.find(x => x.id == id);
   }

   loadAll() {
     const ForecastsUrl = 'http://demo-gateway.morrell.rocks/api/weather/Forecast';

     return this.http.get<Forecast[]>(ForecastsUrl)
      .subscribe( data => {
        this.dataStore.Forecasts = data;
        this._Forecasts.next(Object.assign({}, this.dataStore).Forecasts);
      }, error => {
        console.log('Failed getting Forecasts...');
     });
   }
}

