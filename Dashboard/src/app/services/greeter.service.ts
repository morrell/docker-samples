import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { Forecast } from '../models/forecast';
import { Greeting } from '../models/greeting';

const GreetingUrl = 'http://demo-gateway.morrell.rocks/api/greeter/hello';

@Injectable({
  providedIn: 'root',
})

export class GreeterService {

  constructor(private http: HttpClient) {}

  SayHello(): Observable<Greeting> {

      return this.http.get<Greeting>(GreetingUrl);
    }
}

