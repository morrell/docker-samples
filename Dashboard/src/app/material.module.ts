import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule, MatButtonModule, MatSidenavModule, MatIconModule, MatListModule, MatTableModule } from '@angular/material';
import { MatPaginatorModule, MatFormFieldModule, MatRadioModule, MatSelectModule, MatInputModule } from '@angular/material';
import { MatDatepickerModule, MatNativeDateModule, MatOptionModule, MatDialogModule } from '@angular/material';
import { MatProgressSpinnerModule, MatSnackBarModule, MatCardModule } from '@angular/material';

@NgModule({

 imports: [
   CommonModule,   MatToolbarModule,   MatButtonModule,   MatSidenavModule,
   MatIconModule,   MatListModule,   MatTableModule,   MatPaginatorModule,
   MatFormFieldModule,   MatRadioModule,   MatSelectModule,   MatInputModule,
   MatDatepickerModule,   MatNativeDateModule,   MatOptionModule,   MatDialogModule,
   MatProgressSpinnerModule,   MatSnackBarModule, MatCardModule
 ],

 declarations: [],

 exports: [
   MatToolbarModule,   MatButtonModule,   MatSidenavModule,   MatIconModule,
   MatListModule,   MatTableModule,   MatPaginatorModule,   MatFormFieldModule,
   MatRadioModule,   MatSelectModule,   MatInputModule,   MatDatepickerModule,
   MatNativeDateModule,   MatOptionModule,   MatDialogModule,   MatProgressSpinnerModule,
   MatSnackBarModule, MatCardModule
 ]
})

export class MaterialModule { }
