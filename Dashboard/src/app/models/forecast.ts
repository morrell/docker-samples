export class Forecast {
    id: Number;
    date: Date;
    temperatures: Number[];
}
