import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSort, MatTableDataSource} from '@angular/material';
import { WeatherService } from '../services/weather.service';
import { Observable } from 'rxjs';
import { Forecast } from '../models/forecast';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css']
})
export class ForecastComponent implements OnInit {
  displayedColumns: string[] = ['id', 'date', 'temperatures'];

  forecasts: Observable<Forecast[]>;

  constructor(private weatherService: WeatherService) {
  }
  @ViewChild(MatSort) sort: MatSort;

  ngOnInit() {
    this.weatherService.loadAll();
    this.forecasts = this.weatherService.Forecasts;
  }
}
