FROM node AS BUILD

WORKDIR /app

COPY package.json /app/

RUN npm install

ARG buildno=1

COPY . .

CMD npm run e2e -- --dev-server-target=